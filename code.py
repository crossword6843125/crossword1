#def parse_grid(grid_strings):
#   return [list(row) for row in grid_strings]

def input(filename: str) -> list[list[str]]:
	with open(filename, 'r') as file:
		return [[char for char in line.strip() if char !=" "] for line in file]

grid = input("raw_input.txt")

def crossword(grid: list[list[str]]) -> list:
    ls = []
    number =1
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            ans1, ans2 = False, False
            if (row == 0 or grid[row-1][col] == '#') and (row+1 < len(grid) and grid[row+1][col] != '#'):
                ans1 = True
            if (col ==0 or grid[row][col-1] == '#') and (col+1 < len(grid[row]) and grid[row][col+1] != '#'):
                ans2 =True
            if grid[row][col] != '#' :
                if ans1 or ans2:
                    ls.append((row, col, number))
                    number+=1
    return ls

print(crossword(grid))
