def parse_grid(grid_strings):
    return [list(row) for row in grid_strings]
